local module_folder = lfs.writedir() .. [[Scripts\training_map\]]
package.path = module_folder .. "?.lua;" .. package.path

local groupbuilder = require("groupbuilder.groupbuilder")
local unitbuilder = require("groupbuilder.unitbuilder")
local dcs = require("dcs")

local navalEasy = {
        ["Oil platform"] = 5
}