local module_folder = lfs.writedir() .. [[Scripts\training_map\]]
package.path = module_folder .. "?.lua;" .. package.path

local groupbuilder = require("groupbuilder.groupbuilder")
local grouploader = require("groupbuilder.grouploader")
local unitbuilder = require("groupbuilder.unitbuilder")
local dcs = require("dcs")
dcs.Log("Starting range load.")

local easyRanges = require("ranges-easy")
local navalRanges = require("ranges-naval")

local Ranges = {}

RangesInUse = {}
RangeDespawnTimer = 3600 -- 1 hour.
SmokeRefresh = {}

SmokeColors = {
  trigger.smokeColor.Green,
  trigger.smokeColor.Red,
  trigger.smokeColor.White,
  trigger.smokeColor.Orange,
  trigger.smokeColor.Blue
}

function RandomInList(list)
    local keys = {}
    for k in pairs(list) do table.insert(keys, k) end
    return list[keys[math.random(#keys)]]
end

function FilterTable(t, filter)
  local out = {}
  for k,v in pairs(t) do
    if filter(v) then out[k] = v end
  end
  return out
end
function Find(t, f)
  for k, v in ipairs(t) do
    if f(v, k) then return v, k end
  end
  return nil
end


function GetFreeZone(zonelist)
  local filteredZones = FilterTable(zonelist, function(zone)
    local idx = Find(RangesInUse, function(r)
      return r["zone"] == zone
    end)
    return idx == nil
  end)
  return RandomInList(filteredZones)
end

function smokeGroup(grp)
  local smokeColor = RandomInList(SmokeColors)
  dcs.smokeAtGroup(grp, smokeColor)
  return smokeColor
end

function setSmokeRefresh(smokeConfig)
  table.insert(SmokeRefresh, smokeConfig)
end

function smokeConfigForRange(rangeGroup, smokeColor)
  if rangeGroup == nil then return nil end
  local smokeConfig = {}
  smokeConfig["position"] = dcs.groupCoords(rangeGroup)
  smokeConfig["color"] = smokeColor
  smokeConfig["groupName"] = rangeGroup:getName()
  return smokeConfig
end


function disableRangeSmokeRefresh(rangeGroup)
  local _, idx = Find(SmokeRefresh, function(smoke)
    return smoke["groupName"] == rangeGroup
  end)
  table.remove(SmokeRefresh, idx)
end

function clearRange(playerGroupName)
  local range = RangesInUse[playerGroupName]
  if range == nil then return end
  dcs.Log("Clearing range for group [" .. playerGroupName .. "].")
  local rangeGroupName = range["group"]
  if rangeGroupName ~= nil then
    local rangeGroup = Group.getByName(rangeGroupName)
    if rangeGroup then
      rangeGroup:destroy()
      dcs.Log("Destroying range group")
    end
    if range["jtacGroup"] then
      dcs.Log("Range had jtac. Destroying.")
      range["jtacGroup"]:destroy()
    end
    disableRangeSmokeRefresh(rangeGroupName)
    dcs.Log("Disabled the auto-smoke refresh for the range too")
  end
  RangesInUse[playerGroupName] = nil
  dcs.Log("Done clearing up the range")
end

function scheduleRangeDespawn(playerGroup)
  local playerGroupName = playerGroup:getName()
  local rangeGroupName = RangesInUse[playerGroupName]["group"]
  mist.scheduleFunction(function()
    local rangeGroup = Group.getByName(rangeGroupName)
    if rangeGroup ~= nil then
      dcs.Log("Clearing range for group [" .. playerGroupName .. "] due to timeout.")
      clearRange(playerGroupName)
      -- after an hour delay, player may no longer be in the same group or playing
      local currentPlayerGroup = Group.getByName(playerGroupName)
      if currentPlayerGroup and currentPlayerGroup:isExist() then
        dcs.MessageToGroup(currentPlayerGroup:getID(), "Your range is been despawned after a 1 hour timeout", 10)
      end
    end
  end, nil, timer.getTime() + RangeDespawnTimer)
end

function groupHasRange(grp)
  local r = RangesInUse[grp:getName()]
  return r ~= nil
end

function despawnRangeForGroup(group)
  local range = RangesInUse[group:getName()]
  if range == nil then
    dcs.MessageToGroup(group:getID(), "You don't have a range assigned to you. Try spawning one first.", 5)
  else
    clearRange(group:getName())
    dcs.MessageToGroup(group:getID(), "Your range has been despawned.", 5)
  end
end

function destroyAllRanges(group)
  for owner, range in pairs(RangesInUse) do
    dcs.Log("destroying range for [" .. owner .. "]: [" .. range["group"] .. "]")
    Group.getByName(range["group"]):destroy()
  end
end

function positionString(grp, pos)
  return dcs.CoordsForGroup(grp, pos)
end

function rangeInfoText(grp, range)
  local response = ""
  local pos = dcs.groupCoords(Group.getByName(range["group"]))
  local spawnTime = range["spawnTime"]
  response = response .. "Target location: " .. dcs.CoordsForGroup(grp, pos) .. "\n"
  response = response .. "Smoke Color: " .. dcs.SmokeColorString(range["smokeColor"]) .. "\n"
  response = response .. "This range will despawn in ".. spawnTime - timer.getTime() + RangeDespawnTimer .." seconds.\n"
  if range["laserCode"] then
    response = response .. "Active JTAC using laser code: " .. range["laserCode"] .. "\n"
  end
  return response
end

function sendGroupRangeInfo(grp)
  local range = RangesInUse[grp:getName()]
  if range == nil then
    dcs.MessageToGroup(grp:getID(), "You don't currently have a range assigned to you.", 5)
  else
    local text = rangeInfoText(grp, range)
    dcs.MessageToGroup(grp:getID(), text, 30)
  end
end

function setGroupInvisible(grp)
  SetGroupCommand(grp, "SetInvisible", true)
end

function setGroupImmortal(grp)
  SetGroupCommand(grp, "SetImmortal", true)
end

function SetGroupCommand(grp, command, val)
  if grp == nil then return nil end
  dcs.Log("Setting " .. command .. " to " .. tostring(val) .. " for group " .. grp:getName())
  local ctlr = grp:getController()
  if ctlr then
    ctlr:setCommand({
        id = command,
        params = {
          value = val
        }
      })
  end
end

function spawnJtacForGroup(grp)
  dcs.Log("Spawning JTAC")
  local rangeInfo = RangesInUse[grp:getName()]
  if not rangeInfo then
    dcs.Log("No group to spawn jtac for. exiting.")
    dcs.MessageToGroup(grp:getID(), "You don't have a range assigned for a JTAC. Spawn one first.", 5)
    return
  end
  if not rangeInfo["allowJtac"] then
    dcs.MessageToGroup(grp:getID(), "Your range does not support JTAC units.", 5)
    return
  end
  if rangeInfo["jtacGroup"] then
    dcs.Log("Already have a JTAC. skipping")
    dcs.MessageToGroup(grp:getID(), "You already have a JTAC unit for your range. You cannot spawn another one", 5)
    return
  end
  local jtacGroup = groupbuilder.New("JTAC-"..grp:getName()):withCountry("USA")
  local rangeZone = rangeInfo["zone"]
  local spawnPoint = mist.getRandomPointInZone(rangeZone)
  dcs.Log("JTAC Spawn Point: " .. mist.utils.tableShow(spawnPoint))
  jtacGroup = jtacGroup:withUnits(
      {unitbuilder.New("M1043 HMMWV Armament")}
    ):atPosition(spawnPoint)
  dcs.Log("Trying to spawn JTAC in: " .. rangeZone)
  local spawnedGroup = jtacGroup:spawn()
  spawnedGroup = Group.getByName(spawnedGroup.name)
  mist.scheduleFunction(function()
    -- Grimes suggested doing this in a scheduled function
    -- It apparently can cause crashes trying to set these if you don't.
    setGroupInvisible(spawnedGroup)
    setGroupImmortal(spawnedGroup)

  end, {}, timer.getTime() + 1)
  local laserCode = table.remove(ctld.jtacGeneratedLaserCodes, 1)
  table.insert(ctld.jtacGeneratedLaserCodes, laserCode)
  ctld.JTACAutoLase(spawnedGroup:getName(), laserCode)
  rangeInfo["jtacGroup"] = spawnedGroup
  rangeInfo["laserCode"] = laserCode
  RangesInUse[grp:getName()] = rangeInfo
  dcs.MessageToGroup(grp:getID(), "Your JTAC is active. Laser code " .. laserCode)
end

function RangeConfig(name, zonePattern, templateFile)
    local zones = dcs.ZonesWithPattern(zonePattern)
    if #zones == 0 then
        dcs.Log("No zones found with pattern '" .. zonePattern .. "'.")
    end
    dcs.Log("Loading groups from file [".. templateFile .."]")
    local groupbuilders = grouploader.LoadFile(templateFile)
    dcs.Log("Loaded groups from file [".. templateFile .."]")
    for k,v in pairs(groupbuilders) do
        dcs.Log("Group: " .. v.name)
    end
    return {
        name = name,
        zones = zones,
        groupbuilders = groupbuilders
    }
end

-- local easyRanges = RangeConfig("Easy", "RANGE_EASY_", module_folder .. "/range_templates/ground/Easy.stm")
local mediumRanges = RangeConfig("Medium", "RANGE_MEDIUM_", module_folder .. "/range_templates/ground/Medium.stm")

function SpawnDynamicRange(rangeConfig, initiatingGroup)
  if groupHasRange(initiatingGroup) then
    dcs.MessageToGroup(initiatingGroup:getID(), "You already have a range assigned. You can't spawn another until you have either completed the last range or despawned it via the Radio Menu.", 5)
    return
  end
  dcs.Log("Spawning " .. rangeConfig.name .. " range...")
  local spawned_grp = SpawnRange(rangeConfig, initiatingGroup)
  local smokeColor = smokeGroup(spawned_grp)
  RangesInUse[initiatingGroup:getName()]["smokeColor"] = smokeColor
  local smokeConfig = smokeConfigForRange(spawned_grp, smokeColor)
  setSmokeRefresh(smokeConfig)
  local response = rangeConfig.name .. " range spawned on your behalf.\n"
  response = response .. rangeInfoText(initiatingGroup, RangesInUse[initiatingGroup:getName()])
  dcs.MessageToGroup(initiatingGroup:getID(), response, 30)

  scheduleRangeDespawn(initiatingGroup)
  dcs.Log("Done spawning " .. rangeConfig.name .. " range")
end

function SpawnRange(rangeConfig, initiatingGroup)
    local rangeGroup = RandomInList(rangeConfig.groupbuilders)
    dcs.Log("RangeConfig Zones: " .. mist.utils.tableShow(rangeConfig.zones))
    local zone = GetFreeZone(rangeConfig.zones)
    dcs.Log("Group to be spawned is " .. rangeGroup.name .. ". In range " .. zone)
    local position = mist.getRandomPointInZone(zone)
    local spawnedGroup = rangeGroup:withCountry("CJTF_RED"):atPosition(position):spawn()
    spawnedGroup = Group.getByName(spawnedGroup.name)
    RangesInUse[initiatingGroup:getName()] = {
        ["zone"] = zone,
        ["group"] = spawnedGroup:getName(),
        ["spawnTime"] = timer.getTime(),
        ["owner"] = initiatingGroup:getName(),
        ["allowJtac"] = true
    }
    return spawnedGroup
end

function addRadioMenus(grp)
  local spawnRangeBaseMenu = dcs.GroupMenu(grp:getID(), "Ranges", nil)
  dcs.GroupCommand(grp:getID(), "My Range Info", spawnRangeBaseMenu, function()
    sendGroupRangeInfo(grp)
  end)
  dcs.GroupCommand(grp:getID(), "Spawn Easy", spawnRangeBaseMenu, function()
    SpawnDynamicRange(easyRanges, grp)
  end)
  dcs.GroupCommand(grp:getID(), "Spawn Medium", spawnRangeBaseMenu, function()
    SpawnDynamicRange(mediumRanges, grp)
  end)
  dcs.GroupCommand(grp:getID(), "Give me a JTAC", spawnRangeBaseMenu, function()
    spawnJtacForGroup(grp)
  end)
  dcs.GroupCommand(grp:getID(), "Despawn My Range", spawnRangeBaseMenu, function()
    despawnRangeForGroup(grp)
  end)
  --dcs.GroupCommand(grp:getID(), "Despawn All Ranges", spawnRangeBaseMenu, function()
  --  destroyAllRanges()
  --end)
end

local _radioBirthHandler = function(event)
  if event.id ~= world.event.S_EVENT_BIRTH then return end
  if not event.initiator then return end
  if not event.initiator.getGroup then return end
  local grp = event.initiator:getGroup()
  if grp then
    for i,u in ipairs(grp:getUnits()) do
      if u:getPlayerName() and u:getPlayerName() ~= "" then
        addRadioMenus(grp)
      end
    end
  end
end
mist.addEventHandler(_radioBirthHandler)

function rangeDeathCheck()
  local res, err = pcall(function()
    for ownerName, range in pairs(RangesInUse) do
      local rangeGroupName = range["group"]
      if not dcs.GroupIsAlive(rangeGroupName) then
        -- Group has been killed. Clear it up and inform the owning group.
        mist.scheduleFunction(function()
          local ownerGroup = Group.getByName(ownerName)
          clearRange(ownerName)
          if ownerGroup and ownerGroup:isExist() then
            dcs.MessageToGroup(ownerGroup:getID(), "Your range has been destroyed! Congratulations.")
          end
        end, nil, timer.getTime() + 1)
      end
    end
    return true
  end)
  if not res then
    dcs.Log("Error checking range groups for status: " .. err)
  end
  mist.scheduleFunction(rangeDeathCheck, nil, timer.getTime() + 5)
end
mist.scheduleFunction(rangeDeathCheck, nil, timer.getTime() + 5)


dcs.Log("Done Range Load")

return Ranges
