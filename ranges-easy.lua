local module_folder = lfs.writedir() .. [[Scripts\training_map\]]
package.path = module_folder .. "?.lua;" .. package.path

local groupbuilder = require("groupbuilder.groupbuilder")
local unitbuilder = require("groupbuilder.unitbuilder")
local dcs = require("dcs")

local unitlayout = {
        ["Ural-375"] = 7,
        ["Land_Rover_101_FC"] = 4
}

local group = groupbuilder.New("Easy"):withCountry("CJTF_RED")
local units = {}
for unitType, unitCount in pairs(unitlayout) do
        for i=1,unitCount do
                local u = unitbuilder.New(unitType)
                local offset = dcs.RandomOffset(20,150)
                u = u:withOffset(offset):withHeading(math.random(0, 6))
                table.insert(units, u)
        end
end

return {
        groupbuilders = { group:withUnits(units) },
        name = "Easy",
        zones = dcs.ZonesWithPattern("RANGE_EASY_")
}