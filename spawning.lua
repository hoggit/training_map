local dcs = require "dcs"
local logger = dcs.Logger("Spawning")
local Spawning = {}
local KEEPALIVE_RESPAWN_TIME = 60

Spawning.KeepAliveGroups = {}

Spawning.KeepAliveDetails = function(groupName, groupbuilder)
        return {
                groupName = groupName,
                groupbuilder = groupbuilder
        }
end

Spawning.KeepAlive = function(groupbuilder)
        dcs.Log("Setting KeepAlive on groupbuilder: " .. groupbuilder.name)
        local group = groupbuilder:spawn()
        local keepalivedetails = Spawning.KeepAliveDetails(group.name, groupbuilder)
        Spawning.KeepAliveGroups[group.name] = keepalivedetails
end

local function checkGroupKeepAlive(group_name)
        local keepalive = Spawning.KeepAliveGroups[group_name]
        if not keepalive then return end
        if not dcs.GroupIsAlive(group_name) then
                mist.scheduleFunction(function()
                        logger.Log("Group " .. group_name .. " is spawning its replacement")
                        Spawning.KeepAlive(keepalive.groupbuilder)
                end, {}, timer.getTime() + KEEPALIVE_RESPAWN_TIME)
                Spawning.KeepAliveGroups[group_name] = nil
                logger.Log("Group " .. group_name .. " has been wiped out. Triggering respawn")
        end
end

local function pollAllKeepAlives()
        for group_name,_ in pairs(Spawning.KeepAliveGroups) do
                checkGroupKeepAlive(group_name)
        end
end

Spawning.DeathEventHandler = function(event)
        if not (event.id == world.event.S_EVENT_CRASH or event.id == world.event.S_EVENT_DEAD or event.id == world.event.S_EVENT_ENGINE_SHUTDOWN) then return end
        if not event.initiator then return end
        if not event.initiator.getGroup then return end
        if not event.initiator:getGroup() then return end
        local group_name = event.initiator:getGroup():getName()
        checkGroupKeepAlive(group_name)
end

mist.addEventHandler(Spawning.DeathEventHandler)
mist.scheduleFunction(pollAllKeepAlives, {}, 1, 5)
logger.Log("Respawning system loaded.")

return Spawning