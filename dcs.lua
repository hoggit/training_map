local dcs = {}

dcs.Log = function(msg)
        env.info("Training Map -- " .. msg)
end

dcs.Logger = function(loggerName)
  return {
    Log = function(str) dcs.Log(loggerName .. " - " .. str) end
  }
end

-- Adds intelligence to the way scripting errors in DCS are handled.
 HandleError = function(err)
    dcs.Log(err)
    dcs.Log(debug.traceback())
    return err
end

--- Adds basic try/catch functionality
-- @param func unsafe function to call
-- @param catch the function to call if func fails
local try = function(func, catch)
    return function()
      local r, e = xpcall(func, HandleError)
      if not r then
        return catch(e)
      end
      return r
    end
end


-- Message Text to players at the top right.
dcs.MessageToGroup = function(groupId, text, displayTime, clear)
        displayTime = displayTime or 5
        clear = clear or false
        trigger.action.outTextForGroup(groupId, text,displayTime, clear)
end

dcs.MessageToAll = function(text, displayTime)
        displayTime = displayTime or 5
        trigger.action.outText(text, displayTime)
end
-- ends Message Text

-- F10 Radio Menu
dcs.GroupCommandAdded = {}
dcs.GroupCommand = function(group, text, parent, handler)
    if dcs.GroupCommandAdded[tostring(group)] == nil then
        dcs.GroupCommandAdded[tostring(group)] = {}
    end
    if not dcs.GroupCommandAdded[tostring(group)][text] then
        local callback = try(handler, function(err) dcs.MessageToAll("Error in group command" .. err, 60) end)
        missionCommands.addCommandForGroup(group, text, parent, callback)
        dcs.GroupCommandAdded[tostring(group)][text] = true
    end
end

dcs.GroupMenuAdded = {}
dcs.GroupMenu = function(groupId, text, parent)
    if dcs.GroupMenuAdded[tostring(groupId)] == nil then
        dcs.GroupMenuAdded[tostring(groupId)] = {}
    end
    if not dcs.GroupMenuAdded[tostring(groupId)][text] then
        dcs.GroupMenuAdded[tostring(groupId)][text] = missionCommands.addSubMenuForGroup(groupId, text, parent)
    end
    return dcs.GroupMenuAdded[tostring(groupId)][text]
end

-- Ends F10 Radio Menu

--- Returns a string of coordinates in a format appropriate for the planes of the
--- provided group. i.e. if the group contains F/A-18Cs then we'll return Degrees Minutes Seconds format
--@param grp The group the coordinates are going to be presented to
--@param position The position (table of x,y,z) coordinates to be translated.
--@return String containing the formatted coordinates. Returns an empty string if either grp or position are nil
dcs.CoordsForGroup = function(grp, position)
  if grp == nil or position == nil then return "" end
  local u = grp:getUnit(1)
  if not u then return "" end -- Can't get any units from the group to inspect.

  local groupPlaneType = u:getTypeName()
  return dcs.CoordForPlaneType(groupPlaneType, position)
end

--- Given a plane type and position, return a string representing the position in a format useful for that planetype.
--@param planeType String indicating the DCS plane type. See Unit.getTypeName() in DCS Scripting docs.
--@param position The position (table of x,y,z) coordinates to be translated
--@return String of coordinates formatted so they can be useful for the given planeType
dcs.CoordForPlaneType = function(planeType, pos)
  local lat,long = coord.LOtoLL(pos)
  local dms = function()
    return mist.tostringLL(lat, long, 0, "")
  end
  local ddm = function()
    return mist.tostringLL(lat, long, 3)
  end
  local mgrs = function()
    return mist.tostringMGRS(coord.LLtoMGRS(lat,long),4)
  end
  local endms6 = function()
    return mist.tostringLL(long, lat, 2, "")
  end
  --If it's not defined here we'll use dms.
  local unitCoordTypeTable = {
    ["Ka-50"] = ddm,
    ["M-2000C"] = ddm,
    ["A-10C"] = mgrs,
    ["AJS37"] = endms6,
    ["F-14B"] = ddm
    -- Everything else will default to dms. Add things here if we need exclusions.
  }
  local f = unitCoordTypeTable[planeType]
  if f then return f() else return dms() end
end

--- Returns the location of the first unit in a given group.
-- If the group is nil, this function returns nil.
-- @param grp The group you want coordinates for.
-- @return Vec3 The group's first unit's coordinates, or nil if the group is nil
dcs.groupCoords = function(grp)
  if grp ~= nil then
    return grp:getUnits()[1]:getPosition().p
  end
  return nil
end

--- Returns a textual smoke name based on the provided enum
-- @param a trigger.smokeColor enum
-- @return the English word as a string representing the color of the smoke. i.e. trigger.smokeColor.Red returns "Red"
dcs.SmokeColorString = function(smokeColor)
  if smokeColor == trigger.smokeColor.Green then return "Green" end
  if smokeColor == trigger.smokeColor.Red then return "Red" end
  if smokeColor == trigger.smokeColor.White then return "White" end
  if smokeColor == trigger.smokeColor.Orange then return "Orange" end
  if smokeColor == trigger.smokeColor.Blue then return "Blue" end
end

--- Returns if Group object is alive.
-- This will catch some of the edge cases that the more common functions miss.
-- @param group Group
-- @return True if Group is indeed, alive.  False otherwise.
dcs.GroupIsAlive = function(group)
    local grp = nil
    if type(group) == "string" then
        grp = Group.getByName(group)
    else
        grp = group
    end
    if grp and grp:isExist() and grp:getSize() > 0 then return true else return false end
end

--- Starts a smoke beacon at the specified group's location
-- @param grp The group to smoke. Will be placed on or near the first unit.
-- @param smokeColor The trigger.smokeColor enum value to use. Defaults to White smoke
dcs.smokeAtGroup = function(grp, smokeColor)
  local pos = dcs.groupCoords(grp)
  if smokeColor == nil then smokeColor = trigger.smokeColor.White end
  trigger.action.smoke(pos, smokeColor)
end

--Finds all zones that begin with a pattern and
--end with a number, sequentially contiguous from 1.
dcs.ZonesWithPattern = function(pattern)
    local zones = {}
    local idx = 1
    dcs.Log("Searching zones for pattern '"..pattern.."'...")
    while (true) do
        local z = trigger.misc.getZone(pattern .. idx)
        if (z == nil) then
            dcs.Log("No more zones.")
            break
        end
        dcs.Log("Found zone: " .. pattern .. idx)
        table.insert(zones, pattern .. idx)
        idx = idx + 1
    end
    return zones
end

dcs.RandomOffset = function(absMin,absMax)
    local modifier_x = (math.random(0,1)*2.0) - 1.0
    local modifier_y = (math.random(0,1)*2.0) - 1.0
    return {
        x = math.random(absMin, absMax) * modifier_x,
        y = math.random(absMin, absMax) * modifier_y,
    }
end

dcs.scriptsDir = function() return lfs.writedir() .. "Scripts\\" end

dcs.Log("Hoggit DCS Module loaded.")
return dcs